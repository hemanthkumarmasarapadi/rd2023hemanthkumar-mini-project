package com.epam.TestLoginStepDefs;

import com.epam.pojos.UserData;
import com.epam.pojos.UserResponseData;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

import java.util.Map;

import static com.epam.constants.APIConstants.BASEURI;
import static io.restassured.RestAssured.given;

public class TestLoginStepdefs {
    private final Logger logger= LogManager.getLogger();
    Response response;
    @Given("I make an API call with BASE url")
    public void iMakeAnAPICallWithBASEUrl() {
        RestAssured.baseURI= BASEURI;
    }

    @When("I gave an userdetails as request body and make a POST request")
    public void iGaveAnUserdetailsAsRequestBodyAndMakeAPOSTRequest(Map<String,String> userdetails)
    {
        String username=userdetails.get("userName");
        String password=userdetails.get("Password");
        UserData userData=new UserData();
        userData.setUserName(username);
        userData.setPassword(password);
        response=given().contentType(ContentType.JSON)
                .body(userData)
                .when()
                .post("Account/v1/User").then().extract().response();

    }

    @Then("I validate the reponse code as {int}")
    public void iValidateTheReponseCodeAs(int reponseCode) {
        Assert.assertEquals(response.getStatusCode(),reponseCode);
        logger.info("response code asserted");
    }

    @Then("I validate the {string} response body")
    public void iValidateTheResponseBody(String username) {
        UserResponseData userResponseData=response.as(UserResponseData.class);
        Assert.assertEquals(userResponseData.getUsername(),username);
        Assert.assertTrue(userResponseData.getBooks().isEmpty());

        logger.info("user body asserted");
    }

}
