package com.epam.TestLoginStepDefs;

import com.epam.drivers.PicoContainer;
import com.epam.pages.LoginPage;
import com.epam.pages.UserPortal;
import com.epam.utilities.SeleniumUtilities;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;

public class TestSignupFunctionalityStepdefs {
    private final Logger logger= LogManager.getLogger();
    private LoginPage loginPage;
    private UserPortal userPortal;

    public TestSignupFunctionalityStepdefs(PicoContainer container)
    {
        loginPage=container.getPicoContainer().getComponent(LoginPage.class);
        userPortal=container.getPicoContainer().getComponent(UserPortal.class);
    }
    @Given("I navigated to login page")
    public void iNavigatedToLoginPage() {
        loginPage.navigateToLoginPage();
    }

    @When("I entered {string} and {string}")
    public void iEnteredAnd(String username, String password) {
        loginPage.enterUserName(username);
        loginPage.enterUserPassword(password);
    }

    @And("I clicked on login button")
    public void iClickedOnLoginButton() {

        loginPage.clickOnLoginButton();
    }

    @Then("I Should able to see {string} as profile name")
    public void iShouldAbleToSeeAsProfileName(String username) {

        Assert.assertEquals(userPortal.getUsername(),username);
        logger.info("user profile name asserted");
    }
}
