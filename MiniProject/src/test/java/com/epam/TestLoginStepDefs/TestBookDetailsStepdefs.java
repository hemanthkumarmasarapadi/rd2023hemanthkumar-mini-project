package com.epam.TestLoginStepDefs;

import com.epam.drivers.PicoContainer;
import com.epam.pages.BooksPage;
import com.epam.pages.LoginPage;
import com.epam.pojos.BooksResponse;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.asserts.SoftAssert;
import static com.epam.constants.APIConstants.BOOKS_BASE_URI;
import static io.restassured.RestAssured.given;

public class TestBookDetailsStepdefs {

    private LoginPage loginPage;

    private BooksPage booksPage;


    public TestBookDetailsStepdefs(PicoContainer container)
    {
        loginPage=container.getPicoContainer().getComponent(LoginPage.class);
        booksPage=container.getPicoContainer().getComponent(BooksPage.class);
    }

    Response response;
    @Given("I make an API call of books url")
    public void iMakeAnAPICallOfBooksUrl() {
        RestAssured.baseURI= BOOKS_BASE_URI;
    }
    @When("I got the book details by sending GET Request")
    public void iGotTheBookDetailsBySendingGETRequest() {
        response=given()
                .when()
                .get("v1/Books").then().extract().response();

    }

    @And("I launched the books url")
    public void iLaunchedTheBooksUrl() {
        loginPage.navigateToBooksPage();

    }

    @Then("I validate the captured details")
    public void iValidateTheCapturedDetails() {
        BooksResponse booksResponse=response.as(BooksResponse.class);
        int index;
        for (index=0;index<8;index++)
        {
            SoftAssert softAssert=new SoftAssert();
            softAssert.assertEquals(booksPage.getBookTitles().get(index).getText(),booksResponse.getBooks().get(index).getTitle());
            softAssert.assertEquals(booksPage.getBookAuthors().get(index).getText(),booksResponse.getBooks().get(index).getAuthor());
            softAssert.assertEquals(booksPage.getBookPublishers().get(index).getText(),booksResponse.getBooks().get(index).getPublisher());
            softAssert.assertAll();
        }
    }
}
