package com.epam;


import com.epam.drivers.DriverSingleton;
import com.epam.pages.BasePage;
import io.cucumber.java.After;
import io.cucumber.java.Before;


public class Hooks extends BasePage {

    private Hooks()
    {

    }

    @Before("@login or @books")
    public static void setUp()
    {
        try {
            driver= DriverSingleton.getDriver();
            driver.manage().window().maximize();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    @After("@login or @books")
    public static void tearDown()
    {
        driver.close();
        driver.quit();
    }
}
