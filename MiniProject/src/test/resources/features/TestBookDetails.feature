@books
Feature: validate books details
  Scenario: capturing book details and validating
    Given I make an API call of books url
    When I got the book details by sending GET Request
    And I launched the books url
    Then I validate the captured details