Feature:Checking Login Validation
  Scenario Outline: I tested signup through API
    Given I make an API call with BASE url
    When I gave an userdetails as request body and make a POST request
      | Field    | Value      |
      | userName | <username> |
      | Password | <password> |
    Then I validate the reponse code as 201
    Then I validate the "<username>" response body
    Examples:
      | username                 | password |
      | hemanth kumar masarapadi | Mhk@1049 |

  @login
  Scenario Outline: I tested the login functionality
      Given I navigated to login page
      When I entered "<username>" and "<password>"
      And I clicked on login button
      Then I Should able to see "<username>" as profile name
      Examples:
        | username        | password |
        | hemanth kumar masarapadi | Mhk@1049 |
