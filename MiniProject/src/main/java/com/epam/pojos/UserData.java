package com.epam.pojos;

import lombok.Data;

@Data
public class UserData {

    private String userName;
    private String password;
}
