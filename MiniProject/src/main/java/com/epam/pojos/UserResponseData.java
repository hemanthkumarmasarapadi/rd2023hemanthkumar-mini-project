package com.epam.pojos;

import lombok.Data;

import java.util.List;
@Data
public class UserResponseData {

    private String userID;
    private String username;

    private List<String> books;
}
