package com.epam.pojos;


import lombok.Data;

import java.util.List;

@Data
public class BooksResponse {
    private List<BookDetails> books;
}
