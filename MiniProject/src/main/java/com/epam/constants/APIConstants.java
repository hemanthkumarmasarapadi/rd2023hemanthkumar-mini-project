package com.epam.constants;

public class APIConstants {
    private APIConstants()
    {

    }

    public static final String BASEURI ="https://demoqa.com/";

    public static final String BOOKS_BASE_URI ="https://demoqa.com/BookStore/";
}
