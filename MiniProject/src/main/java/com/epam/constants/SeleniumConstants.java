package com.epam.constants;

public class SeleniumConstants {
    private SeleniumConstants()
    {

    }
    public static final String LOGIN_URL="https://demoqa.com/login";

    public static final String BOOKS_PAGE_URL="https://demoqa.com/books";
}
