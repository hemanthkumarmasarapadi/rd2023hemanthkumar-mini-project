package com.epam.drivers;
import com.epam.constants.Browers;
import com.epam.factorymethods.DriverManager;
import com.epam.factorymethods.MyChromeDriver;
import com.epam.factorymethods.MyEdgeDriver;
import com.epam.factorymethods.MyFireFoxDriver;
import org.openqa.selenium.WebDriver;

public class DriverSingleton {

    private static WebDriver driver;

    private DriverSingleton()
    {

    }

    public static synchronized void setDriver(Browers browers)
    {
        switch (browers) {
            case CHROME -> {
                DriverManager driverManager = new MyChromeDriver();
                driver = driverManager.create();
            }
            case EDGE -> {
                DriverManager driverManager = new MyEdgeDriver();
                driver = driverManager.create();
            }
            case FIREFOX -> {
                DriverManager driverManager = new MyFireFoxDriver();
                driver = driverManager.create();
            }

        }
    }

    public static synchronized WebDriver getDriver()
    {
        setDriver(Browers.CHROME);
        return driver;
    }

}
