package com.epam.drivers;

import com.epam.pages.BooksPage;
import com.epam.pages.LoginPage;
import com.epam.pages.UserPortal;
import org.picocontainer.DefaultPicoContainer;
import org.picocontainer.MutablePicoContainer;

public class PicoContainer {
    protected MutablePicoContainer container;

    public PicoContainer()
    {
        container=new DefaultPicoContainer();
        container.addComponent(LoginPage.class);
        container.addComponent(UserPortal.class);
        container.addComponent(BooksPage.class);
    }

    public MutablePicoContainer getPicoContainer()
    {
        return container;
    }

}
