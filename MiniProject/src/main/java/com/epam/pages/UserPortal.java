package com.epam.pages;

import com.epam.utilities.SeleniumUtilities;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;



public class UserPortal extends BasePage{
    private final Logger logger= LogManager.getLogger();
    public UserPortal()
    {
        PageFactory.initElements(BasePage.driver,this);
    }
    @FindBy(id = "userName-value")
    private WebElement userName;

    public String getUsername()
    {
        SeleniumUtilities.elementToBeVisible(userName);
        logger.info("returning username as text");
        return userName.getText();
    }

}
