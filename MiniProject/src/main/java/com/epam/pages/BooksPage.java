package com.epam.pages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class BooksPage extends BasePage{
    private final Logger logger= LogManager.getLogger();

    public BooksPage()
    {
        PageFactory.initElements(BasePage.driver,this);
    }
@FindBy(xpath = "//div[@class='rt-tbody']/div/div/div[3]")
private List<WebElement> bookAuthors;
@FindBy(xpath = "//div[@class='rt-tbody']/div/div/div[2]")
    private List<WebElement> bookTitles;
@FindBy(xpath = "//div[@class='rt-tbody']/div/div/div[4]")
private List<WebElement> bookPublishers;




    public List<WebElement> getBookAuthors()
    {
        logger.info("returning Books Authors as a list");
          return bookAuthors;
    }
    public List<WebElement> getBookTitles()
    {
        logger.info("returning Books titles as a list");
        return bookTitles;
    }

    public List<WebElement> getBookPublishers()
    {
        logger.info("returning Books publisher as a list");
        return bookPublishers;
    }

}
