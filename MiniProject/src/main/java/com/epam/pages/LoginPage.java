package com.epam.pages;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static com.epam.constants.SeleniumConstants.BOOKS_PAGE_URL;
import static com.epam.constants.SeleniumConstants.LOGIN_URL;

public class LoginPage extends BasePage{
    private final Logger logger= LogManager.getLogger();
    public LoginPage()
    {
        PageFactory.initElements(BasePage.driver,this);
    }
@FindBy(id = "userName")
    private WebElement userName;
@FindBy(id = "password")
    private WebElement password;
@FindBy(id = "login")
private WebElement loginButton;

    public void navigateToLoginPage()
    {
        driver.get(LOGIN_URL);
        logger.info("launched the Login Url");
    }

    public void enterUserName(String username)
    {
        userName.sendKeys(username);
        logger.info("username entered");
    }

    public void enterUserPassword(String userPassword)
    {
        password.sendKeys(userPassword);
        logger.info("password entered");
    }


    public void clickOnLoginButton()
    {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor)driver;
        javascriptExecutor.executeScript("arguments[0].click()", loginButton);
        logger.info("login button clicked");
    }
    public void navigateToBooksPage()
    {
        driver.get(BOOKS_PAGE_URL);
        logger.info("navigated to the book page url");
    }

}
